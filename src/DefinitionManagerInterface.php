<?php

namespace Drupal\integro;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Defines a plugin manager.
 */
interface DefinitionManagerInterface extends PluginManagerInterface {

}
