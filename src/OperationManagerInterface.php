<?php

namespace Drupal\integro;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Defines a plugin manager.
 */
interface OperationManagerInterface extends PluginManagerInterface {

}
