<?php

namespace Drupal\integro\Plugin\Integro\Client;

use Drupal\integro\ClientInterface;

/**
 * @IntegroClient(
 *   id = "integro_default",
 *   label = "Default",
 * )
 */
class ClientDefault extends ClientBase implements ClientInterface {

}
