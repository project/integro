<?php

namespace Drupal\integro\Plugin\Integro\Definition;

use Drupal\integro\DefinitionInterface;

/**
 * @IntegroDefinition(
 *   id = "default",
 *   label = "Default",
 * )
 */
class DefinitionDefault extends DefinitionBase implements DefinitionInterface {

}
