<?php

namespace Drupal\integro\Plugin\Integro\Integration;

use Drupal\integro\IntegrationInterface;

/**
 * @IntegroIntegration(
 *   id = "default",
 *   label = "Default",
 * )
 */
class IntegrationDefault extends IntegrationBase implements IntegrationInterface {

}
