<?php

namespace Drupal\integro\Plugin\Integro\Operation;

use Drupal\integro\OperationInterface;

/**
 * @IntegroOperation(
 *   id = "default",
 *   label = "Default",
 * )
 */
class DefaultOperation extends OperationBase implements OperationInterface {

}
